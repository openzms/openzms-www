<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# openzms-www

This repository contains the source for the https://www.openzms.net web
site.

## Preview

```
npm run docs:dev
```

## Build

```
npm run docs:build
```

Build output will be in `site/.vitepress/dist/`.
