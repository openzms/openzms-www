
# Contact Us

To receive notifications of OpenZMS releases and other updates, please join
the https://groups.google.com/g/openzms-announce mailing list.  Refer to the
[Google Groups join instructions](https://support.google.com/groups/answer/1067205?hl=en)
as necessary.

If you are interested in using or contributing to OpenZMS, or have general
comments or questions, please contact the maintainers at info@openzms.net .
