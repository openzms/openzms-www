---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# OpenZMS API

Each of the core OpenZMS services provide a "northbound", user-facing
RESTful API.  Each API is described by an
[openapi](https://www.openapis.org/) specification, which defines and
documents both endpoints and the JSON data models they produce and consume.

  * `zms-identity`:
    https://gitlab.flux.utah.edu/openzms/zms-identity/-/blob/main/api/northbound/openapi.json
  * `zms-zmc`:
    https://gitlab.flux.utah.edu/openzms/zms-zmc/-/blob/main/api/northbound/openapi.json
  * `zms-dst`:
    https://gitlab.flux.utah.edu/openzms/zms-dst/-/blob/main/api/northbound/openapi.json
  * `zms-alarm`: https://gitlab.flux.utah.edu/openzms/zms-alarm/-/blob/main/api/northbound/openapi.json
