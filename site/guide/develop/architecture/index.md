---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# OpenZMS Architecture

The following diagram illustrates the OpenZMS implementation.  OpenZMS
employs a centralized spectrum management approach, where spectrum consumers
operate under explicit, dynamic delegations of authority from OpenZMS
services.  This design ensures that OpenZMS can remain in control of an RDZ:
it can implement flexible policy-based sharing while still providing
protections from harmful interference.  OpenZMS is designed and anticipated
to manage municipal to regional-scale deployments, with anticipated
extensions supporting hierarchical deployments that provide chains of
spectrum authority, delegation, and observability.

![OpenZMS Architecture](/images/powder_rdz_implementation3.png "OpenZMS Architecture"){data-zoomable}

OpenZMS is built as a set of containerized, cloud-native services to support
horizontal scalability for large analysis and prediction workloads, and to
facilitate extensibility via third-party services. Each service that
provides parts of the ZeAL API does so over a RESTful JSON ”northbound”
interface.  Users authenticate via API tokens and are authorized via
role-based access control (RBAC). Internally, services communicate over
trusted gRPC service APIs and event streams.

The core `identity`, `zmc`, `dst`, and `alarm` services are implemented in
`golang`; the `propsim-tirem` and `propsim-winprop` propagation simulation
services are implemented in Python.  The core services each store data in a
relational Postgres database, and the `dst` further uses the PostGIS
extensions to support raster storage of propagation simulation maps and
geospatial indexes and queries. The OpenZMS web UI, built using the [Vue.js]
and [Nuxt.js] frameworks, exposes OpenZMS’s core abstractions. The UI
displays zone status, live measurement graphs, and propagation simulation
maps as web map tiles generated and cached by a Geoserver instance, attached
to the dst services’s PostGIS database.


## OpenZMS Zone Abstration Layer (ZeAL)

The ZeAL APIs provide the external, publicly-available, “northbound”
interface through which organizations and their members participate in
spectrum sharing activities within an RDZ. We refer to a participating
organization as an Element: Elements are the unit of teaming and
collaboration within an organization. Elements provide and update OpenZMS
with the configuration of resources that they use within the RDZ (e.g.,
radio transmitters and receivers), and resources that they offer to the RDZ
(e.g., spectrum, radio monitors, infrastructure)—either through an automated
Element-side service, or via manual ZeAL API invocations when necessary.
Users are members of one or more Elements, and may be granted one or more
Roles in a number of Elements to use spectrum or observe its use; or to
perform administrative and operational activities within an Element, such as
changing an antenna associated with a radio. We expect that many users of
RDZ spectrum will not themselves be OpenZMS Users: instead, these Element
members may access RDZ resources through user accounts within their
organization (Element), and the Element will offer RDZ resources to its own
users, via its own abstractions, by consuming the the ZeAL APIs.


### Core Data Model

OpenZMS defines a comprehensive data model intended to capture operating
characteristics of RDZ radios that consume spectrum.  OpenZMS models
*Radios* as devices with one or more *RadioPorts*, each of which is
connected to an *Antenna*. Radios model the higher-level, generic properties
of a device, such as FCC identifier, model, and more. RadioPorts define
additional details of operation, such as operating bands, tx/rx modes,
maximum power level, and attached antenna azimuth, elevation angle, and
location.  Antennas define the properties of a specific model of antenna,
and can be described via simple properties like gain and beam width; or by
detailed radiation patterns (e.g., the widely-used MSI format). OpenZMS does
not operate these Radios; this information is necessary to enable effective
prediction (e.g., propagation simulation) and analysis of measurements
(e.g., interference, model accuracy, and more).

OpenZMS’s monitoring abstractions build upon these base
abstractions. Elements define *Monitors*, associated with particular
RadioPorts, which provides detailed reception characteristics to OpenZMS
analysis services. Monitors report *Observations*, which contain radio
measurement data. The OpenZMS design does not dictate specific measurement
and data formats, and accepts processed, indexed, and analyzed information
(e.g., occupancy data, power-spectral density), or raw sample data (e.g.,
SigMF). Our goal is to enable many kinds of services within OpenZMS via
horizontally-scalable Observation analysis pipelines. Analysis services may
attach additional processed or learned information to Observations as
Annotations (e.g., an interference and risk analysis).

To delegate spectrum to OpenZMS to manage, Elements create *Spectrum*
objects. Each Spectrum object consists of start and end constraints, radio
constraints (band, power, area of operation, maximum leakage allowed outside
areas of operation, etc), and usage policy (restrictions on particular
Elements, priority, required approvals, use models such as when-unoccupied).
Elements may revoke Spectrum delegations at any time, which will result in
revocation of current and future grants allocated within the revoked
Spectrum object.  To obtain spectrum from OpenZMS, Elements create *Grant*
objects, which consist of the same kinds of start, duration, and radio
constraints, and may be associated with one or more RadioPorts. Grant
requests may be under-specified across these constraints, allowing OpenZMS
to determine a ”best fit” allocation of spectrum to the Grant.


## OpenZMS Services

The OpenZMS ZeAL API is implemented by several services that provide secure,
RESTful endpoints.  All services communicate internally over a trusted,
RPC-based messaging layer, designed to facilitate high-throughput,
low-latency messaging and reactive, event-based analysis pipelines.


### Identity Service

The `zms-identity` service provides the Element, User, Role, and Token
abstractions and operations. Users can scope Tokens with subsets of their
Roles to restrict the set of authorized operations. All OpenZMS services
register with the identity service, which maintain


### Zmc (Zone Management Controller) Service

The `zms-zmc` service provides the Radio,
Monitor, Spectrum, Grant, and related abstractions— it is the primary API
endpoint for Elements to populate OpenZMS with radio device information; to
provide spectrum for OpenZMS to manage; and for Elements to reserve spectrum
via the Grant abstraction. The zmc service currently hosts the spectrum
scheduler, which allocates Spectrum to Grants, querying other services as
required by policy (e.g. the dst service’s occupancy data and propagation
simulation maps).  The zmc service revokes Grants when notified by the alarm
service that they are in violation of operating constraints.


### DST (Digital Spectrum Twin) Service

The dst (digital spectrum twin) service provides the Observation,
Collection, Annotation, and Propsim (propagation simulation)
abstractions. Conceptually, it operates as a dualpurpose system, functioning
both as a predictive engine and a data analytics hub. This service indexes
records of RF measurements, spectrum usage, and radio device data, and
provides a predictive query interface. These queries serve two purposes: 1)
determining occupancy, such as verifying the availability of X MHz of
spectrum for radio Y at power level Z, at a specific time T, and for a
duration D; and 2) estimating a transmission power range for radio Y at time
T, ensuring emissions remain below power P outside the RDZ.  The dst’s
analysis capabilities empower it to extrapolate intelligence from short-term
observations to identify and understand long-term, spatial spectrum usage
trends.  The dst service stores Observation data in persistent file storage
and indexes within appropriate databases (e.g., geospatial, relational,
time-series) to support fast queries. It can invoke propagation simulation
services on-demand, but can also simulate in expectation of future usage as
new Radio and Spectrum objects are created and updated by Elements.

### Propsim (Propagation Simulation) Services

The dst service acts as a client to propsim (propagation simulation)
services, which generate expected received signal strength maps and other
geospatial features for one or more transmitters operating in a given band
and power level. These predictions are the basis to facilitate simultaneous,
deconflicted shared spectrum use. OpenZMS defines an extensible RPC-based
propsim job service, and through this interface, the dst service can run
parameterized propagation simulations to obtain maps with received signal
strength data. The dst service caches and indexes these maps in its PostGIS
database to facilitate geographic sharing under a variety of constraints.

### Alarm Service

The alarm service analyzes and responds to unexpected interference reports
that can occur due to spatial and temporal changes not captured even with
sophisticated planning and a multi-dimensional DST. OpenZMS uses monitor
observations, along with historical data from the DST, to manage spectrum
access for consumers, revoking access from probable interferers. Credible
reports from interference events and monitoring data will be used to update
the DST for future risk assessments. The primary concern for incumbents is
minimizing interference events. This is especially critical for sensitive
applications like radio astronomy that have a very small window of
acceptable interference. OpenZMS provides a real-time response to unexpected
interference reports caused by other RDZ consumers.
