---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# OpenZMS Development

::: warning Upcoming

OpenZMS is in a pre-beta stage and is under heavy development; its design
and APIs are still evolving; and this documentation is a work in progress.
You can track progress and the current roadmap by checking the overall
project issue list: https://gitlab.flux.utah.edu/groups/openzms/-/issues

:::

The OpenZMS software is available at https://gitlab.flux.utah.edu/openzms .
OpenZMS is a collection of cloud-native services, each defining data models
and public and internal APIs to support spectrum management.  The core
services are

  * `zms-identity`: https://gitlab.flux.utah.edu/openzms/zms-identity
  * `zms-zmc`: https://gitlab.flux.utah.edu/openzms/zms-zmc
  * `zms-dst`: https://gitlab.flux.utah.edu/openzms/zms-dst
  * `zms-alarm`: https://gitlab.flux.utah.edu/openzms/zms-alarm

