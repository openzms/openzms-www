---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# OpenZMS Internal APIs

OpenZMS services communicate internally over a collection of [gRPC
services](https://gitlab.flux.utah.edu/openzms/zms-api).
Services are defined in `.proto` files in [the zms-api
repository](https://gitlab.flux.utah.edu/openzms/zms-api/-/tree/main/proto/zms?ref_type=heads).
Auto-generated documentation for these service definitions is available
[here](https://gitlab.flux.utah.edu/openzms/zms-api/-/tree/main/docs/zms?ref_type=heads).
We auto-generate bindings for
[`golang`](https://gitlab.flux.utah.edu/openzms/zms-api/-/tree/main/go?ref_type=heads)
and
[`python`](https://gitlab.flux.utah.edu/openzms/zms-api/-/tree/main/python?ref_type=heads);
instructions for these bindings are [available
here](https://gitlab.flux.utah.edu/openzms/zms-api/-/tree/main/docs?ref_type=heads).

Here is an list of the services with pointers to their definition and
documentation.

  * [`zms/identity/v1/identity.proto`](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/proto/zms/identity/v1/identity.proto?ref_type=heads):
    - Description: the identity service provides authentication and
      authorization services, tokens, role-based access control, and service
      directory capabilities
    - Documentation: [service](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/identity/v1/identity.md?ref_type=heads#identity), [overview](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/identity/v1/identity.md?ref_type=heads)

  * [`zms/zmc/v1/zmc.proto`](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/proto/zms/zmc/v1/zmc.proto?ref_type=heads):
    - Description: the zmc service provides spectrum and radio support: it
      acts as an information base and provides scheduling support for
      spectrum and monitors.
    - Documentation: [service](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/zmc/v1/zmc.md?ref_type=heads#zmc), [overview](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/zmc/v1/zmc.md?ref_type=heads)

  * [`zms/dst/v1/dst.proto`](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/proto/zms/dst/v1/dst.proto?ref_type=heads):
    - Description: the Digital Spectrum Twin service for a ZMS: stores
observations, indexes data, propagation simulation maps, and provides a
query interface over observations and predicted maps.
    - Documentation: [service](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/dst/v1/dst.md?ref_type=heads#dst), [overview](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/dst/v1/dst.md?ref_type=heads)

  * [`zms/alarm/v1/alarm.proto`](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/proto/zms/alarm/v1/alarm.proto?ref_type=heads):
    - Description: the alarm service for a ZMS: processes Observations,
      Violations, and Interference events, and recommends corrective actions to
      the zmc service's Grants.
    - Documentation: [service](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/alarm/v1/alarm.md?ref_type=heads#alarm), [overview](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/alarm/v1/alarm.md?ref_type=heads)

  * [`zms/propsim/v1/propsim.proto`](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/proto/zms/propsim/v1/propsim.proto?ref_type=heads):
    - Description: the propsim JobService provides a general,
      parameterizable job run interface, and supports a variety of output
      formats (e.g. geotiff maps, raw data series). In OpenZMS, this API is
      typically consumed by the DST (Digital Spectrum Twin) service to run
      propagation simulations for specific radio ports in the Zone.
    - Documentation: [service](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/propsim/v1/propsim.md?ref_type=heads#jobservice), [overview](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/propsim/v1/propsim.md?ref_type=heads)

## Events

Similarly, each service provides a `Subscribe` method that allows a consumer
to register to receive events matching a filter expression as a response
stream.  Each service defines its own `Event` type as a protobuf message
containing service-specific information, but must include the generic
`EventHeader` message defined in the generic `zms/event/v1` service
descriptor:

  * [`zms/event/v1/event.proto`](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/proto/zms/event/v1/event.proto?ref_type=heads):
    - Description: the event service descriptor provides a generic
      EventHeader, EventFilter, service types, and common event types.
      Other services rely on these common definitions.
    - Documentation:
	  * [EventFilter](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/event/v1/event.md?ref_type=heads#eventfilter)
	  * [EventHeader](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/event/v1/event.md?ref_type=heads#eventheader)
      * [EventSourceType](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/event/v1/event.md?ref_type=heads#eventsourcetype)
	  * [EventType](https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/event/v1/event.md?ref_type=heads#eventtype)
