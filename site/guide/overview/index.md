---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# What is OpenZMS?

OpenZMS is a prototype automatic spectrum sharing and management system for
radio dynamic zones.  OpenZMS provides mechanisms to share electromagnetic
(radio-frequency) spectrum between experimental or test systems and existing
spectrum users, and between multiple experimental systems.  We are building
and deploying OpenZMS within the context of the [POWDER mobile wireless
testbed](https://www.powderwireless.net) in Salt Lake City, Utah, part of
the NSF-sponsored [Platforms for Advanced Wireless
Research](https://advancedwireless.org/) program, to create the [POWDER Radio
Dynamic Zone (POWDER-RDZ)](/guide/examples/powder-rdz/), supported by the NSF
[Spectrum Innovation Initiative: National Radio Dynamic Zones
(SII-NRDZ)](https://new.nsf.gov/funding/opportunities/spectrum-innovation-initiative-national-radio/505990)
program.
