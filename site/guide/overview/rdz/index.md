---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# Safe Spectrum Sharing: Radio Dynamic Zones

Spectrum access challenges currently create significant constraints on
experimentation and testing at wireless testbeds. Automatic spectrum sharing
that provides safe access to additional frequencies beyond those reserved
exclusively for testing will relax these constraints, and thus increase the
nation's capacity to conduct wireless research and development. Increasing
this capacity will help accelerate growth and global leadership of the US
communications industry, strengthen academic research into wireless systems,
and benefit other spectrum-dependent sectors such as radar, public safety,
and national defense.

Spectrum sharing is not a panacea, however, and incumbent spectrum users are
rightly deeply concerned about the possible impact of spectrum sharing
approaches on their respective wireless applications.  A concept being
pursued by the research community to explore, and hopefully allay, these
concerns is a *Radio Dynamic Zone* (RDZ). In essence, an RDZ is a spatial
volume at a particular geographic location where wireless experimentation,
and spectrum sharing approaches in particular, can be performed in a
controlled way and specifically in such a manner that the potential impact
on incumbent spectrum users can be reasoned about and monitored so as to
understand and reduce the associated impact and risk. RDZs will be equipped
with the necessary tools, mechanisms, equipment and processes to realize
such safe exploration.
