---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# OpenZMS and [POWDER-RDZ](/guide/examples/powder-rdz/)

As a pathfinder for the National Radio Dynamic Zone concept, OpenZMS and
POWDER-RDZ will help future federal/non-federal spectrum sharing
arrangements assure that spectrum sharing does not negatively impact
existing and incumbent spectrum use.  By building and operating an
end-to-end radio dynamic zone (RDZ), we will validate the RDZ concept and
functionality by performing spectrum sharing experiments and field
studies. The project uses the existing POWDER mobile and wireless testbed as
the physical infrastructure of the RDZ. POWDER's existing radios and other
equipment supports the spectrum sharing experiments and provides part of the
RF sensing functionality needed by the RDZ.

POWDER-RDZ relies on the OpenZMS modular zone management system to manage,
control, and monitor all radio aspects of the RDZ. The project plans to
conduct experiments on spectrum sharing with users outside of
POWDER. Experiments potentially include RDZ shared access to federal,
non-federal, and commercial spectrum, such as coarse- and fine-grained
spectrum sharing with a commercial mobile operator and spectrum sharing with
a weather radar.
