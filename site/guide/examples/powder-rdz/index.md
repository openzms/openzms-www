---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# POWDER-RDZ: the POWDER Radio Dynamic Zone

POWDER-RDZ is a radio dynamic zone being created at the University of Utah's
[POWDER](https://www.powderwireless.net) mobile and wireless network
testbed.  As a full-featured automated testbed for outdoor over-the-air
wireless network research, POWDER natively provides some features of a zone
management system (e.g. spectrum scheduling and monitoring, automatic RF
kill switch on transmit violation, and more).  However, it lacks more
advanced zone management features: parallel same-band spectrum sharing,
detailed network planning, occupancy analysis, risk analysis and automatic
interference report and transmission violation mitigation.

The POWDER-RDZ portal can be accessed at https://rdz.powderwireless.net .

::: info POWDER-RDZ was introduced in this paper at [DySPAN '24](https://dyspan2024.ieee-dyspan.org/):

[POWDER-RDZ: Prototyping a Radio Dynamic Zone using the POWDER platform](https://www.flux.utah.edu/paper/333)

[David Johnson](https://www.flux.utah.edu/profile/johnsond), [Dustin Maas](https://www.flux.utah.edu/profile/dmaas), [Serhat Tadik](https://serhatadik.github.io/), [Alex Orange](https://www.flux.utah.edu/profile/orange), [Leigh Stoller](https://www.flux.utah.edu/profile/stoller), [Kirk Webb](https://www.flux.utah.edu/profile/kwebb), [Basit Awan](https://www.linkedin.com/in/basit-iqbal), [Jacob Bills](https://www.flux.utah.edu/profile/271), Miguel Gomez, [Aarushi Sarbhai](https://users.cs.utah.edu/~aarushis/), [Greg Durgin](https://ece.gatech.edu/directory/gregory-david-durgin)), [Sneha Kumar Kasera](https://kasera.coe.utah.edu/), [Neal Patwari](https://engineering.wustl.edu/faculty/Neal-Patwari.html), [David Schurig](https://faculty.utah.edu/u0738807-David_Schurig/hm/index.hml), and [Jacobus (Kobus) Van der Merwe]()

IEEE International Symposium on Dynamic Spectrum Access Networks (IEEE DySPAN) 2024.

:::

## POWDER-OpenZMS Integration

![POWDER-RDZ Architecture](/images/powder_rdz_examplev2.png "POWDER-RDZ Architecture"){data-zoomable}

POWDER and OpenZMS are separate software systems.  POWDER-RDZ is a
deployment of OpenZMS, and POWDER utilizes OpenZMS spectrum management
services by integrating with the OpenZMS ZEAL northbound APIs.  Using these
APIs, POWDER does the following:

  * Implements the *Provider role*: POWDER delegates some of its accessible
    outdoor spectrum to OpenZMS for management and monitoring
  * Implements the *Consumer role*: when experiments request to use some of
    the delegated spectrum, POWDER requests a spectrum *grant* from OpenZMS
    according to the experiment requirements, and provides that grant to the
    experiment
  * Implements the *Monitor role*: provides the output of POWDER
    over-the-air monitors (power spectral density data) and inline, coupled
    TX monitors

As part of implementing the *Consumer* and *Monitor* roles, POWDER populates
the OpenZMS data model with all necessary information about its radios:
location, antenna, capabilities, and more.  This provides OpenZMS with the
necessary radio metadata to perform on-demand, high-fidelity network
planning to support spectrum sharing, as well as monitoring data to ensure
safe sharing.


## POWDER-RDZ: OpenZMS Zone Status

The following screenshot shows the zone-wide OpenZMS status page.  The map
shows the University of Utah campus area covered by POWDER's FCC Program
Experimental License and Innovation Zone designations and many of the
locations of POWDER radios as blue markers.  The navbar to left of the map
allows users to filter the map display by
  * Spectrum: delegated bands provided to OpenZMS to manage
  * Grants: active/revoked grants scheduled by OpenZMS
  * Radios: transmitters, receivers, monitors

![POWDER-RDZ OpenZMS Status Page](/images/openzms-status.jpg "POWDER-RDZ OpenZMS Status Page"){data-zoomable}

## POWDER-RDZ: OpenZMS Zone Status

The OpenZMS status map shows live observation data from selected monitors.
This is a power spectral density graph produced by the POWDER over-the-air
monitor running on an NI X310 USRP software-defined radio on the `honors`
rooftop, targeted to sweep lower c-band and CBRS.

![POWDER-RDZ OpenZMS Observation](/images/openzms-observation.jpg "POWDER-RDZ OpenZMS C-Band Observation"){data-zoomable}

## POWDER-RDZ: POWDER mobile 5G experiment Setup

This screenshot shows a freshly-instantiated mobile 5G experiment on POWDER,
using the [`srs-outdoor-ota` POWDER
profile](https://www.powderwireless.net/show/PowderTeam/srs-outdoor-ota).
This profile creates a mobile 5G deployment from bare metal: it allocates
one or more of POWDER's densely deployed SDRs with medium-power RF
frontends, campus shuttles carrying POWDER SDRs and COTS UEs, configures
srsRAN 5G base gNodeBs atop them, giving the user a real 5G network
deployment.

![POWDER-RDZ POWDER 5G Experiment](/images/powder-swapin--done.jpg "POWDER-RDZ POWDER 5G Experiment"){data-zoomable}

## POWDER-RDZ: POWDER mobile 5G experiment Using Spectrum

This profile and screenshot use the POWDER-RDZ capabilities.  When the
experiment was created, POWDER requested a grant of *available, unoccupied*
lower c-band spectrum from OpenZMS, and OpenZMS returned a 20MHz grant
around 3480MHz.  POWDER provided that information to the experiment, and the
5G network was created using that spectrum.

In the screenshot below, you can see that the POWDER OTA monitor used by
OpenZMS to select a range of unoccupied spectrum is now showing occupancy at
3480MHz, as the mobile COTS UEs connect to the gNodeB and run throughput
tests.

![POWDER-RDZ OpenZMS 5G Observation](/images/openzms-observation-5g.jpg "POWDER-RDZ OpenZMS C-Band Observation with 5G Base Station"){data-zoomable}

