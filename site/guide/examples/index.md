---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# Example Deployments

OpenZMS has been deployed for pre-release testing at two sites to evaluate
and develop its sharing potential in two different contexts:

  * [POWDER-RDZ](/guide/examples/powder-rdz/): deployed at the University of Utah's POWDER platform
    (https://www.powderwireless.net) to create a prototype radio dynamic
    zone: https://rdz.powderwireless.net
  * [HCRO-RDZ](/guide/examples/hcro-rdz/): deployed at the Hat Creek Radio Observatory (HCRO): https://www.seti.org/hcro
  
Each deployment exercises different OpenZMS features.  The POWDER-RDZ site
focuses on safe coexistence of active transmit experiments with
active-transmit incumbents, using POWDER's Program Experiment License and
Innovation Zone designation.  The HCRO deployment focuses on active transmit
experiments that use spectrum provided by a passive user: a radio astronomy
facility.
