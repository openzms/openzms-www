---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# OpenZMS at HCRO

The [Wireless Interdisciplinary Research
Group](https://www.colorado.edu/lab/wirg/) at the University of Colorado
Boulder, led by [Kevin Gifford](https://www.colorado.edu/cs/kevin-gifford), and the University of Utah and teamed to extend the OpenZMS core
services to support experimentation in passive sensitive-user radio
astronomy use cases, and collaborated with [University of California
Berkeley](https://ral.berkeley.edu/people/david-deboer/) and
[HCRO-SETI](https://seti.org/hcro) to [deploy OpenZMS](/guide/examples/hcro-rdz/)
at the [Hat Creek Radio Observatory](https://seti.org/hcro) (HCRO).  The
Colorado Boulder team contributed support for the TARDYS3 specification to
OpenZMS's grant management subsystems, and integrated the Colorado Boulder
[Radio Frequency Noise
Sensor](https://www.colorado.edu/lab/wirg/research/radio-frequency-noise-sensor)
with OpenZMS's monitor abstractions to provide the OpenZMS digital spectrum
twin and alarm services with data and interference reports and enhance
sharing with sensitive passive spectrum users.  The Utah team extended
OpenZMS to support a variety of new passive and sensitive-user spectrum
sharing use cases.

## HCRO OpenZMS Examples

This screenshot shows the map of the HCRO site as well as managed spectrum,
allocated grants, and several radios.  The large cluster of blue markers are
mainly the individual antennas that comprise the [Allen Telescope
Array](https://www.seti.org/ata); the more scattered markers are the
University of Colorado Boulder RFS Sensors and in some locations co-located
transmitters hosting Utah software-defined radios for active transmission
experiments, although there are additional RFS sensors deployed nearer to
the ATA cluster as well.

The bottom of the screenshot shows a snapshot of kurtosis values computed by
the RFS sensor system, using live data collected from several CU RFS
sensors.  This characterizes the amount of RFI in the 900MHz ISM band,
collected during active transmissions by the Utah SDRs.  Black-outlined data
points represent interference events.

![HCRO OpenZMS Status Page](/images/hcro-openzms-status-crop.jpg "HCRO OpenZMS Status Page"){data-zoomable}

The screenshot below shows an interference event being handled by OpenZMS.
In this example, there is a single priority grant representing the HCRO
sensitive/passive use, and three lower-priority simultaneous grants, one for
each Utah transmitter.  In this case, the OpenZMS `alarm` service paused all
three Utah grants, but then subsequently allowed them to resume when the RFI ceased.

![HCRO OpenZMS Status Page - Interference
Mitigation](/images/hcro-openzms-interference-crop.jpg "HCRO OpenZMS Status Page - Interference Mitigation"){data-zoomable}
