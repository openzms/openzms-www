---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# Contributing to OpenZMS

::: warning Upcoming

OpenZMS's core open-source components are licensed under the Apache License.
We encourage community use and contributions, and plan to use a variant of
the Harmony Contributor License Agreement process.  This will allow
contributors to retain copyright ownership of their changes while also
providing OpenZMS maintainers a license to them.

If you have immediate interest to join the developing OpenZMS community and
contribute to the source code, please [contact us](/contact/).

:::
