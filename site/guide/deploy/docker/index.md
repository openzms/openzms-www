---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# Deploying with Docker Compose <Badge type="warning" text="pre-release" />

This section provides instructions to deploy each service one by one with
[Docker Compose](https://docs.docker.com/compose/), using the local,
insecure non-HTTPS deployment option for ease of use.  Each service provides
a `deploy/docker-compose.yml` service description, and a `deploy/env/`
subdirectory with environment variables you can override to change
configuration.  Each service file uses the same Docker virtual network
names, so you can deploy services one by one using each service's Compose
file, and yet keep them on the appropriate networks for internal
("southbound", gRPC) and external ("northbound", RESTful) API communication.
Each service stores persistent data in `/zms` unless the `DATADIR`
environment variable is overridden in a `.env` file in the service source
code directory.  Services are prefixed with their name (e.g. `zms-frontend`)
and postfixed with one of three deployment options for the service: `-prod`,
`-dev`, and `-local-dev`.  The latter option is only appropriate for local
development and testing on non-public endpoints, since it has default
environment configuration, *including tokens and passwords* that are
committed in the source repositories.

::: danger

Do not run the `-local-dev` variants on public endpoints unless you have
overridden the default password and token configuration, and restricted
access to 
debugging information and control (e.g. the server-side of the
`zms-frontend-local-dev` service will leak information to web clients, so do
not use it on public endpoints).

:::

### Create `DATADIR` for per-service persistent data

```bash
sudo mkdir /zms
sudo chown `id -un` /zms
```

### Deploy the `zms-identity` service

Clone https://gitlab.flux.utah.edu/openzms/zms-identity:

```bash
git clone https://gitlab.flux.utah.edu/openzms/zms-identity
cd zms-identity
```

Build and create the `zms-identity-local-dev` service:

```bash
docker compose -f deploy/docker-compose.yml up -d
```

This will deploy the `zms-identity` service according to the environment
variable configuration in `deploy/env/zms-identity-local-dev.env` ([view in
repository](https://gitlab.flux.utah.edu/openzms/zms-identity/-/blob/main/deploy/env/zms-identity-local-dev.env)).
Look in this file to find two bootstrap tokens for use with the northbound
RESTful API, one for the `admin` account, and another for the `powder`
proto-user account.  This configuration also enables [automatic database
migrations](https://gitlab.flux.utah.edu/openzms/zms-identity/-/blob/main/db/README.md).

View the containers:

```bash
docker compose -f deploy/docker-compose.yml ps
```

Watch the main service logfile:

```bash
docker compose -f deploy/docker-compose.yml logs -f zms-identity-local-dev
```

Save the boostrap admin token into an environment variable:

```bash
export ADMIN_TOKEN=`grep BOOTSTRAP_TOKEN deploy/env/zms-identity-local-dev.env | cut -d= -f2`
```

Construct a URL to the identity service's RESTful API endpoint on its
private IP address:

```bash
export IDENTITY_IP_HTTP=`docker inspect zms-identity-local-dev -f '{{ index . "NetworkSettings" "Networks" "zms-frontend-local-dev-net" "IPAddress"}}'`
export IDENTITY_HTTP=http://${IDENTITY_IP_HTTP}:8000/v1
echo $IDENTITY_HTTP
```

Inspect your admin token via the RESTful API (assumes you have the [`jq`](https://jqlang.github.io/jq/)
tool installed on your system):

```bash
curl -s -k -X GET -H "X-Api-Token: $ADMIN_TOKEN" -H "X-Api-Elaborate: true" ${IDENTITY_HTTP}/tokens/this | jq

```

List the bootstrapped OpenZMS users via the RESTful API:

```bash
curl -s -k -X GET -H "X-Api-Token: $ADMIN_TOKEN" -H "X-Api-Elaborate: true" ${IDENTITY_HTTP}/users | jq
```

List the bootstrapped OpenZMS elements via the RESTful API:

```bash
curl -s -k -X GET -H "X-Api-Token: $ADMIN_TOKEN" -H "X-Api-Elaborate: true" ${IDENTITY_HTTP}/users | jq
```
