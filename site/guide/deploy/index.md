---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# Deploying OpenZMS <Badge type="warning" text="pre-release" />

::: warning Note:

OpenZMS is in a pre-release stage and is under heavy development, and some
services, APIs, and data models are still maturing.  Consequently, there may
still be breaking and backwards-incompatible changes in the near term.
These brief instructions are provided for early adopters.

:::
