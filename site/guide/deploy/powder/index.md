---
outline: deep
---

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# Try OpenZMS in POWDER

The [POWDER platform](https://www.powderwireless.net/) is a mobile and
wireless network testbed on the University of Utah campus.  It provides the
research community with remote access to a variety of radio equipment
(software-defined and COTS) and cloud compute resources.  POWDER supports a
wide variety of experiments, including 5G and beyond, massive MIMO, Open
RAN, spectrum sharing, and many more.  POWDER serves as an important
development environment for OpenZMS.

POWDER provides significant automation and pre-packaging support to deploy
large software stacks on its resources.  A 
[POWDER *profile*](https://docs.powderwireless.net/basic-concepts.html)
is a description of the radio, compute, and network resources required to
run a test or experiment; the software that runs on those resources; and
runtime configuration of both hardware and software.  POWDER users
*instantiate* profiles to create *experiments*.  Powerful profiles typically
provide parameters to customize the experiment, both in terms of resources
and runtime configuration.

We package OpenZMS as a POWDER profile:
https://www.powderwireless.net/show/openzms/zms-profile .  This profile uses
a combination of Ansible and Docker `compose` to deploy the OpenZMS
services.  The source code (e.g. Ansible roles providing runtime
configuration) is available at https://gitlab.flux.utah.edu/openzms/zms-profile .


## Request a POWDER Account and Join the `openzms-demo` Project

Follow the instructions in the POWDER *Getting Started* guide:
https://docs.powderwireless.net/getting-started.html --- but instead of
joining the `TryPowder` project, join the `openzms-demo` project.  At this
point, you will need to wait for POWDER administrators to approve your
account.

While you are awaiting approval, you can read through the `zms-profile`
instructions: https://www.powderwireless.net/show/openzms/zms-profile .

## Create an OpenZMS Experiment

Once approved, you can click the `Instantiate` button on that page, or
instantiate directly via
https://www.powderwireless.net/p/openzms/zms-profile .

This profile deploys the core OpenZMS components and configures an account
so you can experiment with both the frontend and the backend service APIs.
Its simple instructions are designed to familiarize you with some of the
core northbound (user-facing, RESTful) and southbound (internal, gRPC) APIs.


## Run Spectrum Sharing Demos in Your OpenZMS Deployment

::: warning TODO

This feature has not yet been implemented and is blocked on
  * [openzms/zms-profile #1](https://gitlab.flux.utah.edu/openzms/zms-profile/-/issues/1)
  * [openzms/zms-profile #2](https://gitlab.flux.utah.edu/openzms/zms-profile/-/issues/2)

Please monitor these issues or [contact us](/contact/) us for more information.

:::
