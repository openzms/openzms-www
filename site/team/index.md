---
outline: deep
---

# The OpenZMS Team

OpenZMS was developed and is led by the [Flux Research
Group](https://www.flux.utah.edu) at the [University of
Utah](https://www.utah.edu).  The [Wireless Interdisciplinary Research
Group](https://www.colorado.edu/lab/wirg/) at the [University of Colorado
Boulder](https://www.colorado.edu/) has deployed and is contributing to OpenZMS.

OpenZMS originated from the effort to construct
[POWDER-RDZ](/guide/examples/powder-rdz/), an NSF-funded collaborative
research effort between several university teams:

  * University of Utah: [SII-NRDZ: POWDER-RDZ - Spectrum sharing in the POWDER
    platform](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2232463) (PI
    [Kobus Van der Merwe](https://www.flux.utah.edu), Co-PI [Sneha
    Kasera](https://kasera.coe.utah.edu/))
  * Georgia Tech Research Corporation: [SII-NRDZ: POWDER-RDZ - Spectrum
    sharing in the POWDER
    platform](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2232465) (PI
    [Gregory Durgin](https://ece.gatech.edu/directory/gregory-david-durgin))
  * Washington University in St. Louis: [SII-NRDZ: POWDER-RDZ - Spectrum sharing in the POWDER platform](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2232464) (PI
    [Neal Patwari](https://engineering.wustl.edu/faculty/Neal-Patwari.html))

## Collaborations and Contributions

The OpenZMS
[`zms-propsim-tirem`](https://gitlab.flux.utah.edu/openzms/zms-propsim-tirem)
TIREM-based propagation simulation service was originally developed by
members of the [Georgia Tech Propagation
Group](https://www.propagation.gatech.edu/), and modified into the current
service by Georgia Tech and Utah POWDER-RDZ team members.

The [Wireless Interdisciplinary Research
Group](https://www.colorado.edu/lab/wirg/) at the University of Colorado
Boulder, led by [Kevin Gifford](https://www.colorado.edu/cs/kevin-gifford), and the University of Utah and teamed to extend the OpenZMS core
services to support experimentation in passive sensitive-user radio
astronomy use cases, and collaborated with researchers at the [University of California
Berkeley](https://ral.berkeley.edu/people/david-deboer/) and
[HCRO-SETI](https://seti.org/hcro) to [deploy OpenZMS](/guide/examples/hcro-rdz/)
at the [Hat Creek Radio Observatory](https://seti.org/hcro) (HCRO).  The
Colorado Boulder team contributed support for the TARDYS3 specification to
OpenZMS's grant management subsystems, and integrated the Colorado Boulder
[Radio Frequency Noise
Sensor](https://www.colorado.edu/lab/wirg/research/radio-frequency-noise-sensor)
with OpenZMS's monitor abstractions to provide the OpenZMS digital spectrum
twin and alarm services with data and interference reports and enhance
sharing with sensitive passive spectrum users.  The Utah team extended
OpenZMS to support a variety of new passive and sensitive-user spectrum
sharing use cases.
