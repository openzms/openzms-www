---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "OpenZMS"
  text: "Automated spectrum management platform"
  tagline: Intelligent sharing workflows for radio dynamic zones
  actions:
    - theme: alt
      text: Introduction
      link: /guide/overview/
    - theme: alt
      text: Examples
      link: /guide/examples/
    - theme: brand
      text: Try on POWDER
      link: /guide/deploy/powder/
    - theme: alt
      text: Develop
      link: /guide/develop/
  image:
    src: /images/campus-fall-superimposed-openzms-logo-crop-lighter.jpg
    alt: OpenZMS

features:
  - title: Spectrum Sharing
    details: Cloud-native, multi-entity and -user spectrum sharing APIs for active and passive sensitive-user use cases
  - title: Spectrum Intelligence
    details: "Digital Spectrum Twin: planning and monitoring supporting efficient sharing and risk management"
  - title: Spectrum Agility
    details: "Delegate any spectrum for management via policy, collect spectrum usage and metrics via generic observation API"
---

::: info OpenZMS is described in this paper introducing POWDER-RDZ:

[POWDER-RDZ: Prototyping a Radio Dynamic Zone using the POWDER platform](https://doi.org/10.1109/DySPAN60163.2024.10632848)

[David Johnson](https://www.flux.utah.edu/profile/johnsond), [Dustin Maas](https://www.flux.utah.edu/profile/dmaas), [Serhat Tadik](https://serhatadik.github.io/), [Alex Orange](https://www.flux.utah.edu/profile/orange), [Leigh Stoller](https://www.flux.utah.edu/profile/stoller), [Kirk Webb](https://www.flux.utah.edu/profile/kwebb), [Basit Awan](https://www.linkedin.com/in/basit-iqbal), [Jacob Bills](https://www.flux.utah.edu/profile/271), [Miguel Gomez](https://www.linkedin.com/in/mgomez58/), [Aarushi Sarbhai](https://users.cs.utah.edu/~aarushis/), [Greg Durgin](https://ece.gatech.edu/directory/gregory-david-durgin)), [Sneha Kumar Kasera](https://kasera.coe.utah.edu/), [Neal Patwari](https://engineering.wustl.edu/faculty/Neal-Patwari.html), [David Schurig](https://faculty.utah.edu/u0738807-David_Schurig/hm/index.hml), and [Jacobus (Kobus) Van der Merwe]()

[IEEE International Symposium on Dynamic Spectrum Access Networks (IEEE DySPAN) 2024](https://dyspan2024.ieee-dyspan.org/).

:::

<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->
