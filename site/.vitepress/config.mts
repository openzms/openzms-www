import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: ' ',
  description: "The OpenZMS software: an automated approach to spectrum and radio management within a radio dynamic zone.",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: '/images/dark-name-icon.svg',
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/overview/' },
      { text: 'Contact', link: '/contact/' },
      { text: 'Team', link: '/team/' }
    ],
    sidebar: [
      {
        text: 'Guide',
        items: [
          {
            text: 'Overview',
            items: [
              { text: "What is OpenZMS?", link: '/guide/overview/' },
              { text: "What is an RDZ?", link: '/guide/overview/rdz/' },
              { text: "What is POWDER-RDZ?", link: '/guide/overview/powder-rdz/' },
              { text: 'Example Deployments', link: '/guide/examples/' },
              { text: 'Site: POWDER-RDZ', link: '/guide/examples/powder-rdz/' },
              { text: 'Site: Hat Creek Radio Observatory', link: '/guide/examples/hcro-rdz/' }
            ]
          },
          {
            text: 'Getting Started',
            items: [
              {
                text: "Deploy",
                link: '/guide/deploy/',
                items: [
                  { text: "POWDER", link: '/guide/deploy/powder/' },
                  { text: "Docker", link: '/guide/deploy/docker/' },
                ],
              },
              { text: "API", link: '/guide/api/' },
              { text: "Usage", link: '/guide/usage/' },
            ],
          },
          {
            text: 'Develop',
            link: '/guide/develop/',
            items: [
              { text: "Architecture", link: '/guide/develop/architecture/' },
              { text: "API", link: '/guide/develop/api/' },
            ],
          },
          { text: "Contribute", link: '/guide/contribute/' },
          { text: "Contact", link: '/contact/' }
        ]
      },
    ],
    footer: {
      message: 'OpenZMS is supported by the National Science Foundation under <a href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=2232463">Award 2232463</a>.',
      copyright: '© 2024 The University of Utah',
    },
    search: {
      provider: 'local'
    },
    lastUpdated: true
  },
  markdown: {
    image: {
      // image lazy loading is disabled by default
      lazyLoading: true
    }
  },
  head: [
    [
      'script',
      { async: true, src: 'https://www.googletagmanager.com/gtag/js?id=G-G4JT53VBTS' }
    ],
    [
      'script',
      {},
      `window.dataLayer = window.dataLayer || [];
      function gtag(){ dataLayer.push(arguments); }
      gtag('js', new Date());
      gtag('config', 'G-G4JT53VBTS');`
    ]
  ]
})
